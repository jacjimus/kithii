<?php

namespace App\Notifications;

use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Notifications\Messages\MailMessage;
use Illuminate\Notifications\Notification;
use App\Models\SMSNotification;

class TililSMS extends Notification implements shouldQueue
{
    use Queueable;

    /**
     * Create a new notification instance.
     *
     * @return void
     */
    public function __construct($api_key)
    {
        $this->api_key = $api_key;
    }

    /**
     * Get the notification's delivery channels.
     *
     * @param  mixed  $notifiable
     * @return array
     */
    public function via($notifiable)
    {
        return ['sms'];
    }

    /**
     * Get the mail representation of the notification.
     *
     * @param  mixed  $notifiable
     * @return \Illuminate\Notifications\Messages\MailMessage
     */
    public function toMail($notifiable)
    {
        return (new MailMessage)
                    ->line('The introduction to the notification.')
                    ->action('Notification Action', url('/'))
                    ->line('Thank you for using our application!');
    }

    /**
     * Get the array representation of the notification.
     *
     * @param  mixed  $notifiable
     * @return array
     */

     public function toSMS(){
        try {
			$recipients = SMSNotification::where('status', '0')->limit(10)->get();

			// var_dump($recipients);die;
			if (empty($recipients)) {
				return "There are no events due in two days";
			}
			foreach ($recipients AS $res):
				$shortcode = "EPLUS";
				$serviceId = '0';
				$mobile = $res->mobile;
				$message = $res->message;

				$smsdata = array(
					"api_key" => $this->api_key,
					"shortcode" => $shortcode,
					"mobile" => $mobile,
					"message" => $message,
					"serviceId" => $serviceId,
					"response_type" => "json",
				);

				$smsdata_string = json_encode($smsdata);
				//echo $smsdata_string . "\n";

				$smsURL = "https://api.tililsms.com/sms/v3/sendsms";

				//POST
				$ch = curl_init($smsURL);
				curl_setopt($ch, CURLOPT_CUSTOMREQUEST, "POST");

				curl_setopt($ch, CURLOPT_SSL_VERIFYHOST, 0);
				curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, 0);

				curl_setopt($ch, CURLOPT_POSTFIELDS, $smsdata_string);
				curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
				curl_setopt($ch, CURLOPT_HTTPHEADER, array(
						'Content-Type: application/json',
						'Content-Length: ' . strlen($smsdata_string))
				);

				$apiresult = curl_exec($ch);
				$response = json_decode($apiresult);
				//var_dump($response);
				curl_close($ch);
					SMSNotification::where('id' , $res->id)->update(['response'=>$response[0]->status_desc, 'status' => 1 ]);
				endforeach;
		}
		catch(\Exception $e){
			echo $e->getMessage();
		}
     }
    public function toArray($notifiable)
    {
        return [
            //
        ];
    }
}
