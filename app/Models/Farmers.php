<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use App\Models\FarmerGroup;
use App\Models\Villages;

class Farmers extends Model
{
    use SoftDeletes;
    protected $table = "farmers";

    const STATUS_ACTIVE = 'Active';
    const STATUS_INACTIVE = 'Inactive';

    public function attendance()
    {
        return $this->hasMany(Attendance::class, 'farmer_id', 'id');
    }

    public function group()
    {
        return $this->hasOne(FarmerGroup::class,  'id', 'group_id');
    }

    public function village()
    {
        return $this->belongsTo(Villages::class, 'village_id', 'id');
    }

    public function getNameAttribute()
    {
        return $this->first_name.' '.$this->middle_name.' '.$this->last_name ;
    }
}
