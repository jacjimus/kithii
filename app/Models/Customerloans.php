<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Customerloans extends Model
{
    protected $table = 'customer_loans';
    protected  $primaryKey = 'customer_mobile';
}
