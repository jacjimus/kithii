<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Balances extends Model
{
    protected $table = 'customer_balances';
    protected $primaryKey = 'customer_mobile';
}
