<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class SMSNotification extends Model
{
	protected $table  = 'sms_notifications';
}
