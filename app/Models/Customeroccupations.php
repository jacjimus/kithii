<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Customeroccupations extends Model
{
    protected $table = 'customer_occupations';
    protected $primaryKey = 'customer_mobile';
}
