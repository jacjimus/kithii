<?php

namespace App\Models;

use Cartalyst\Sentinel\Users\EloquentUser;
use Illuminate\Database\Eloquent\SoftDeletes;


class User extends EloquentUser
{


    protected $table = "users";

    const  STATUS_ACTIVE = 'Active';
    const  STATUS_DELETED = 'Deleted';
    const  STATUS_BLOCKED = 'Blocked';
    const  STATUS_DORMANT = 'Dormant';

    protected $fillable = [
        'email',
        'password',
        'last_name',
        'first_name',
        'permissions',
        'address',
        'notes',
        'phone',
        'gender'
    ];
    public function payroll()
    {
        return $this->hasMany(Payroll::class, 'user_id', 'id');
    }

    public function getNameAttribute()
    {
        return $this->first_name.' '.$this->last_name;
    }

    public function userswards()
    {
        return $this->belongsToMany(Wards::class , 'users_wards', 'user_id', 'ward_id');
    }

    public function getWardsAttribute(){
        $name = '';
        foreach($this->userswards As $ward){
            $name .= $ward->ward_name . ',';
        }
        return rtrim($name);
    }

    public function getAllPermissions(){
        $roles = $this->roles;
        $permissions = [];
        foreach ($roles as $role) {
            $permissions = array_merge($role->permissions, $permissions); //Merge all  roles permissions
        }
        $permissions = array_merge($this->permissions, $permissions); //Merge  specific user  permissions

        return $permissions; //Return All permissions
    }

}
