<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use App\Models\Wards;
use Illuminate\Database\Eloquent\SoftDeletes;

class Modules extends Model
{
    protected $table = "training_module";

    const STATUS_ACTIVE = 'Active';
    const STATUS_INACTIVE = 'Inactive';

    public function schedules()
    {
        return $this->hasMany(Schedules::class,  'module_id' , 'id');
    }



}
