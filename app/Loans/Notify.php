<?php

namespace App\Loans;

use App\Models\SMSNotification;
use Carbon\Carbon;

class Notify
{

	private $blackbox;
	/**
	 * Create a new instance
	 *
	 * @return void
	 */
	public function __construct()
	{


	}

	/**
	 * Get's events that have subscribers and are due
	 * in 2 days, then sends reminders.
	 *
	 * @return string
	 */
//	public function sendSms($api_key, $api_signature)
//	{
//		$recipients = SMSNotification::where('status',  '0')->limit(10)->get();
//
//     // var_dump($recipients);die;
//		if (empty($recipients)) {
//			return "There are no events due in two days";
//		}
//		foreach ($recipients AS $res):
//			$blackbox = new  Blackbox($api_key, $api_signature);
//			$blackbox->queue_sms($res->mobile, $res->message, "InfoAlert" , "" , "");
//			$blackbox->send_sms();
//			SMSNotification::where('id' , $res->id)->update(['response'=>$blackbox->status, 'status' => 1 ]);
//			endforeach;
//
//
//
//	}

	public function sendSms($api_key)
	{

		try {
			$recipients = SMSNotification::where('status', '0')->limit(100)->get();

			// var_dump($recipients);die;
			if (empty($recipients)) {
				return "There are no events due in two days";
			}
			foreach ($recipients AS $res):
				$shortcode = "EPLUS";
				$serviceId = '0';
				$mobile = $res->mobile;
				$message = $res->message;

				$smsdata = array(
					"api_key" => $api_key,
					"shortcode" => $shortcode,
					"mobile" => $mobile,
					"message" => $message,
					"serviceId" => $serviceId,
					"response_type" => "json",
				);

				$smsdata_string = json_encode($smsdata);
				//echo $smsdata_string . "\n";

				$smsURL = "https://api.tililsms.com/sms/v3/sendsms";

				//POST
				$ch = curl_init($smsURL);
				curl_setopt($ch, CURLOPT_CUSTOMREQUEST, "POST");

				curl_setopt($ch, CURLOPT_SSL_VERIFYHOST, 0);
				curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, 0);

				curl_setopt($ch, CURLOPT_POSTFIELDS, $smsdata_string);
				curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
				curl_setopt($ch, CURLOPT_HTTPHEADER, array(
						'Content-Type: application/json',
						'Content-Length: ' . strlen($smsdata_string))
				);

				$apiresult = curl_exec($ch);
				$response = json_decode($apiresult);
				//var_dump($response);
				curl_close($ch);
					SMSNotification::where('id' , $res->id)->update(['response'=>$response[0]->status_desc, 'status' => 1 ]);
				endforeach;
		}
		catch(\Exception $e){
			echo $e->getMessage();
		}



	}

}
