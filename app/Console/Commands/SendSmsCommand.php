<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;

use App\Loans\Notify;

class SendSmsCommand extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = "send:sms";
    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = "This will send an sms after loan application with details";

    protected $application;

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct(Notify $application)
    {
        parent::__construct();


        $this->application = $application;

    }
    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        $api_key   = getenv("TILIL_API_KEY");
        //$api_signature    = getenv("BLACKBOX_API_SIGNATURE");



        $this->application->sendSms($api_key);
    }
}
