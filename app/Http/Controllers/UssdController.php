<?php
namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\Customers;
use App\Models\Customerbalances;
use App\Models\SMSNotification;
use Response;
use App\Helpers\GeneralHelper;


class UssdController extends Controller
{

    public $msisdn;
    public $message;
    public $session_id;
    public $service_code;
    public $response;
    public $channel;
    public $customer;
    public $loan_amount;
    const SPLITTER_CHAR = '*';
    const CPY_NAME = "Bremwork Loans";
    const USSD_CODE = "*483*113#";
    const LOAN_MIN = "KES2,000";
    const LOAN_MAX = "KES70,000";
    const TILL_NO = "5281215";
    const ACC_TYPE = 'Till';
    const ACC_NO = '9201873201';  //550483201
    const PAYBILL = '100400';  // 553100400558
    const CUSTOMER_CARE = '0111857174';  // 553100400558

    public function __construct() {
        $this->channel = '22';

    }


    public function index(Request $request){

        $this->msisdn = $request->get('phoneNumber');
        $this->message = $request->get('text');
        $this->session_id = $request->get('sessionId');
        $this->service_code = $request->get('serviceCode');
        $customer = Customers::find($this->msisdn);
        $this->customer =$customer;

        // Remove the USSD CODE and Channel

        if($this->message != $this->channel)
            $this->message = $this->str_replace_first($this->channel . '*', '', $this->message);
        //echo $this->message;die;

        /*
         * Split the text var by the *
         */
        // if (strpos($this->message, self::SPLITTER_CHAR) !== false) {
        $this->split_text = explode(self::SPLITTER_CHAR ,  $this->message);


        /*
         * After registration, Initialize the inputs replacing the registration parts
         */

        if(preg_match('/^[a-zA-Z]+\*[a-zA-Z]+\*[0-9]+\*[1-4]\*[1-4]\*?.+$/', $this->message)){
            $this->message = preg_replace('/^[a-zA-Z]+\*[a-zA-Z]+\*[0-9]+\*[1-4]\*[1-4]\*/', '', $this->message);

        }
        /*
         * for every back | Home enrty pick all the inputs after the 0 entry
         */
        if(preg_match('/^.+\*(0)\*?.+$/', $this->message)){
            $this->message = preg_replace('/^.+\*(0)\*/', '', $this->message);

        }
        /*
         * Go back one step by removeing *lastinput from the user input
         */
        if(preg_match('/^(0)$/', end($this->split_text))) {
            // $this->message = str_replace( '*'. end($this->split_text) , '', $this->message);
            $this->message = '';
        }
        /*
         * Logout by enterring 00
         */
        if(preg_match('/^(00)$/', end($this->split_text))) {
            $this->response = "END Thank you for using our USSD service. Welcome back  by dialing " . self::USSD_CODE .' ' . self::CPY_NAME;
        }
        if($this->message == "" || $this->message == $this->channel):

            //Check if customer is registered
            if($this->customer == null):
                $this->response = "CON Welcome to ". self::CPY_NAME." \n Enter your first name";
            else:
                $this->response = "CON Welcome " . $this->customer->first_name . " to ". self::CPY_NAME.". \n 1. Apply Loan \n 2. Repay-Loan \n 3. Check Loan status  \n 4. My Account \n 5. Refer a friend \n 00. Logout";

            endif;
        /*
        Suspend service by uncommenting the below line
        */
        //$this->response = "END Welcome to ". self::CPY_NAME." \n.We have suspended the service for 48hours . Call 0758852845 for assistance.";
        /*
         * User enters first name
         */
        elseif(preg_match('/^[a-zA-Z]+$/', $this->message)):

            if($this->customer == null)
                $customer = new Customers;
            $customer->first_name =  $this->message;
            $customer->mobile =  $this->msisdn;
            $customer->save();

            $this->response = "CON Enter your last name";
        /*
         * User enters last name
         */
        elseif(preg_match('/^[a-zA-Z]+\*[a-zA-Z]+$/', $this->message)):
            if($this->customer == null)
                $customer = new Customers;
            $customer->last_name =  end($this->split_text);
            $customer->save();

            $this->response = "CON Enter your ID number";
        /*
         * User enters ID Number
         */
        elseif(preg_match('/^[a-zA-Z]+\*[a-zA-Z]+\*[0-9]+$/', $this->message)):
            if($this->customer == null)
                $customer = new Customers;
            $customer->id_number =  end($this->split_text);
            $customer->save();
            if($customer->save()):
                //USSDsessions::find($this->session_id)->delete();
                $balance = new \App\Models\Balances();
                $balance->customer_mobile = $this->msisdn;
                $balance->amount = 0;
                $balance->save();
            endif;
            $this->response = "CON Select your occupation \n";
            $occupations = \App\Models\Occupations::where('status', 1)->get();
            foreach($occupations As $occ):
                $this->response .= $occ->id . '. '. $occ->occupation . "\n";
            endforeach;
        /*
         * User to select occupation
         */
        elseif(preg_match('/^[a-zA-Z]+\*[a-zA-Z]+\*[0-9]+\*[1-4]$/', $this->message)):
            $occupation = new \App\Models\Customeroccupations;
            $occupation->customer_mobile =  $this->msisdn;
            $occupation->occupation_id =  end($this->split_text);
            $occupation->save();
            $this->response = "CON Select your monthly earning range \n";
            $earnings = \App\Models\Earnscale::where('status', 1)->get();
            foreach($earnings As $earn):
                $this->response .= $earn->id . '. '. $earn->earnings_per_month . "\n";
            endforeach;
        /*
         * User to select earning range
         */
        elseif(preg_match('/^[a-zA-Z]+\*[a-zA-Z]+\*[0-9]+\*[1-4]$/', $this->message)):
            $occupation = new \App\Models\Customeroccupations;
            $occupation->occupation_id =  end($this->split_text);
            $occupation->save();
            $this->response = "CON Select your Monthly earning range \n";
            $earnings = \App\Models\Earnscale::where('status', 1)->get();
            foreach($earnings As $earn):
                $this->response .= $earn->id . '. '. $earn->earnings_per_month . "\n";
            endforeach;
        /*
         * User is registered and displayed the main menu
         */
        elseif(preg_match('/^[a-zA-Z]+\*[a-zA-Z]+\*[0-9]+\*[1-4]\*[1-4]$/', $this->message)):
            $earnings =\App\Models\Customeroccupations::find($this->msisdn);
            $earnings->earnings_id =  end($this->split_text);
            $earnings->save();

            $this->response = "CON Welcome " . $this->customer->first_name . " to ". self::CPY_NAME.". \n 1. Apply Loan \n 2. Repay-Loan \n 3. Check Loan status  \n 4. My Account \n 5. Refer a friend \n 00. Logout";

        /*
         * user opts to apply for loan
         */
        elseif($this->message == '1'):
            $this->response = "CON Enter amount to borrow between ". self::LOAN_MIN ." and ". self::LOAN_MAX;
        /*
         * Customer wants to repay loan applied for
         */
        elseif($this->message == '2'):
            $this->response = "CON You have no Active loans! \n 0. Home \n 00. Logout ";

        elseif($this->message == '3'):
            $this->response = "CON You can apply upto a max of ". self::LOAN_MAX .". You need to save with us to qualify 5 times your savings instantly! \n 0. Back \n 00. Exit";
        elseif($this->message == '4'):
            $this->response = "CON Enter your ID Number \n";
        elseif(preg_match('/^(4)\*[0-9]+$/', $this->message)):
            $bal = Customerbalances::where('customer_mobile', $this->msisdn)->first();
            $this->response = "CON Your savings balance is Ksh.".GeneralHelper::formatMoney($bal->amount)." \n 0. Back \n 00. Exit";
        elseif($this->message == '5'):
            $this->response = "CON Enter mobile no of your friend \n";
        elseif(preg_match('/^(5)\*[0-9]+$/', $this->message)):
            $message = sprintf("Your friend %s recommended you to apply for an Instant Loan. Dial %s on your Safaricom Line.", $this->msisdn, self::USSD_CODE);
                $this->sendSms(end($this->split_text), $message);

            $this->response = "CON Your friend will get sms notification \n";

        /*
         * User enters amount to borrow
         */
        elseif(preg_match('/^(1)\*[0-9]+$/', $this->message)):
            $loans = \App\Models\Customerloans::where('customer_mobile' , $this->msisdn)->where('status' , 1)->get();

            $i = 1;
            if(count($loans) > 0){
                $this->response = "CON You have one pending loan. Select one to process \n";
                foreach($loans as $loan){
                    $this->response .= "$i. Previous loan of KES ". GeneralHelper::formatMoney($loan->amount).  "\n";
                    $i++;
                }
                $this->response .= $i  . " Apply new Loan of KES" . GeneralHelper::formatMoney(end($this->split_text)) ." \n";
            }
            else
                $this->response = "CON Confirm a Loan of KES" . GeneralHelper::formatMoney(end($this->split_text)) ." at Interest of 10% P.M payable within " . GeneralHelper::payableWithin(end($this->split_text)). " \n"
                    . "1. Accept \n2. Cancel";

        elseif(preg_match('/^(1)\*[0-9]+\*[1|2]$/', $this->message)):
            $loan = \App\Models\Customerloans::where('customer_mobile' , $this->msisdn)->where('status' , 1)->first();

            $i = 1;
            if(count((array)$loan) > 0){
                if( end($this->split_text) == 1 ){ // User has a pending loan and chooses to apply the old loan
                    $this->response = $this->saveStatement($loan->amount / 5);

                }
                else{  //User has a pending loan and chooses th apply the new loan,
                    // update the loan amount
                    \App\Models\Customerloans::where('customer_mobile' , $this->msisdn)->where('status' , 1)->update(['amount' => $this->split_text[count($this->split_text)-2]]);
                    $this->response = $this->saveStatement($this->split_text[count($this->split_text)-2] / 5);
                    // $this->loan_amount = $this->split_text[count($this->split_text)-2];

                }

            }
            else{ // User doesnt have a pending loan
                if( end($this->split_text) == 1 ){ // User accepts the loan with the interest rate.
                    $model = new \App\Models\Customerloans;
                    $model->customer_mobile = $this->msisdn;
                    $model->amount =  $this->split_text[count($this->split_text)-2];
                    $model->status = 1;
                    $model->save();
                    $this->response = $this->saveStatement($this->split_text[count($this->split_text)-2] / 5);

                }
                else
                    $this->response = "CON You have Cancelled loan application \n 0. Home \n00. Logout";


            }



            /*
             * End of the main IF statement
             */
        endif;

        echo $this->response;

    }

    private function saveStatement($amount){
        $this->loan_amount = $amount * 5;
        if($amount >=200 ){ // avoid spammers
            $message = sprintf("Dear %s, You have applied for KES%s .To process the loan, Save KES%s on our MPESA TILL %s. Call %s for help." , $this->customer->first_name, GeneralHelper::formatMoney($this->loan_amount), GeneralHelper::formatMoney(round($this->loan_amount/5 ,0)),  self::TILL_NO, self::CUSTOMER_CARE  );

            $this->sendSms($this->msisdn, $message);
        }

        if(self::ACC_TYPE == 'Bank'):
            return "END  Go to  Mpesa, Lipa na MPESA, PayBill Option, Business no " . self::PAYBILL . ", Account no use " . self::ACC_NO . " and save KES" . GeneralHelper::formatMoney(round($amount , 0)) . " to get the loan in 5 minutes.";

        else:
            return "END To process your loan within 5 min, Kindly save KES " . GeneralHelper::formatMoney(round($amount , 0)) . " to our TILL NO. " . self::TILL_NO . ". the Loan will be sent to your M-PESA instantly.";
        endif;
    }


    private function str_replace_first($from, $to, $content)
    {
        $from = '/'.preg_quote($from , '/').'/';

        return preg_replace($from, $to, $content, 1);
    }


    private function sendSms($phone , $message){
        $sms = new SMSNotification;
        $sms->mobile = $phone;
        $sms->message = $message;
        $sms->status=0;
        $sms->response= 'waiting';
        $sms->save();
    }

    public function payment(Request $request)
    {
        Customerbalances::where("customer_mobile", $request->sender_phone)->increment('amount', $request->amount);
        $loan = Customerbalances::where("customer_mobile", $request->sender_phone)->first();
        if ($loan <> null):
            if ($loan->amount > $request->amount) {
                $message = sprintf("You have saved Ksh %s. You requested Loan of Ksh %s. Top up Ksh %s to ge the loan now!", GeneralHelper::formatMoney($request->amount), GeneralHelper::formatMoney($loan->amount), GeneralHelper::formatMoney((round($loan->amount / 5, 0) - $request->amount)));
                $this->sendSms($request->sender_phone, $message);
            }
            $message = sprintf("%s has deposited KES%s on the TILL No %s at %s", $request->first_name . " " . $request->last_name, GeneralHelper::formatMoney($request->amount), self::TILL_NO, GeneralHelper::formatDate($request->transaction_timestamp, "H:i P"));
            $this->sendSms("254722410268", $message);

        else:
            if($request->amount < 2000)
            $message = sprintf("%s ,you have registered with Ksh %s. Registration is Ksh 2,000. Top up to get the weekly amount of Ksh 10,000 starting now!", $request->first_name , GeneralHelper::formatMoney($request->amount));
            $this->sendSms($request->sender_phone, $message);

        endif;
    }
}




